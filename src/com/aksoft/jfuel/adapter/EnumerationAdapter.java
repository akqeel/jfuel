
package com.aksoft.jfuel.adapter;

import java.util.Enumeration;
import java.util.Iterator;

/**
 *  {@link Enumerator} adapter for {@link Iterator}.
 *  
 * @author Akqeel
 */
public class EnumerationAdapter implements Enumeration{

    private final Iterator iterator;
    
    public EnumerationAdapter(Iterator iterator){
        this.iterator = iterator;
    }
    
    @Override
    public boolean hasMoreElements() { return iterator.hasNext(); }

    @Override
    public Object nextElement() { return iterator.next(); }
    
    
    
}
