package com.aksoft.jfuel.lifecycle;

import com.aksoft.jfuel.service.RequestHandler;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import javax.xml.parsers.ParserConfigurationException;

/**
 * Entry point for the application.
 * Server life cycle is handled here. When the server is
 * started a new Thread is spawned which listens on a port
 * for client requests in a continuous loop. When the server
 * is stopped, the loop exits. 
 * 
 * @author Akqeel
 */
public class LCManager{
    
    /* Port to listen for requests */
    private static final int PORT = 8080;
    private ServerSocket serverSocket;
    
    /* Life cycle manager dependancies. */
    private final ApplicationInflator applicationInflator;
    private final RequestHandler requestHandler;
    
    
    /** 
     * Controls the server state. When started, {@link Thread} 
     * listens on PORT for client requests.
     */
    private Thread  thread;
    private boolean isRunning;
    
  
    
    /** 
     * Default constructor. {@link LCManager} dependencies are initialized here 
     *
     * @throws IOException
     * @throws ParserConfigurationException
     */
    public LCManager() throws IOException, ParserConfigurationException {
        applicationInflator = new ApplicationInflator();
        requestHandler = new RequestHandler();
    } 
    
    
    
    /**
     * Starts the server. First {@link ApplicationInflator) 
     * inflates all WAR files it finds. Then the thread
     * listening in for client requests is started.
     * 
     * @return the running state of the server
     * @throws IOException
     */
    protected boolean start() throws IOException{
        System.out.println("starting JFuel....");
        serverSocket = new ServerSocket(PORT);
        applicationInflator.inflate();
        thread = new Thread(new PortListener());
        thread.start();
        System.out.println("JFuel running...");
        return isRunning = true;
    }
    
    
    
    /**
     * Stops the server. The thread listening for 
     * client requests is interrupted, and the port
     * is closed.
     * 
     * @return the running state of the server
     * @throws IOException
     */
    protected boolean stop() throws IOException{
        System.out.println("stopping JFuel...");
        if(isRunning && thread != null && thread.isAlive()){ //Check if the thread is alive.
           isRunning = false;
           thread.interrupt();
           if(!serverSocket.isClosed()){ //Close the server socket.
               serverSocket.close();   }
           System.out.println("JFuel shutdown successfully");
        }
        return !isRunning;
    }
    
    
    /**
     * Restarts the server. 
     * @return the running state of the server
     * @throws IOException
     */
    protected boolean restart() throws IOException{
       if(stop()){
          start();
       } 
       return isRunning;
    }
    
    
    /** Stops the server and then exits the application. */
    protected void shutdown(){
        try{ 
            stop();
        } catch (IOException ex){
            System.out.println("Failed to stop server. Forcing shutdown");
        }   System.exit(0);
    }

    
    /**
     * The {@link Runnable} which listens for client 
     * requests. When a request is received, the {@link Socket}
     * for that request is delegated to the {@link RequestHandler}
     */
    private class PortListener implements Runnable{

        @Override 
        public void run() {
    
            while(isRunning){
                try {
                    Socket socket = serverSocket.accept();
                    requestHandler.handleRequest(socket);
                    Thread.sleep(100);

                } catch (IOException ex) {
                    System.out.println("Failed to accept request : " + ex.toString());

                } catch (InterruptedException ex) {}
            }
        }
    }
    
    
    /**
     * Entry point for the application. A new instance 
     * of {@link LCManager} is created and started. 
     */
    public static void main(String[] args) {
        try {
            LCManager lcManager = new LCManager();
            lcManager.start();
        
        } catch (ParserConfigurationException | IOException ex) {
            System.out.println("\nFailed to start server: the following exception was raised");
            System.out.println(ex.toString());
        }
    }
    
}
