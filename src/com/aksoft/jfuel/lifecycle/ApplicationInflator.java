
package com.aksoft.jfuel.lifecycle;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

/**
 * Inflates WAR files located at the srcDir and 
 * places the content in the destDir. The {@link ApplicationInflator}
 * is called during server startup.
 * 
 * @author Akqeel
 */
public class ApplicationInflator {
    
    /** Destination directory where WAR files are inflated to */
    public static final String destDir = "E:\\jfuel\\dest";
    
    /** Source directory WAR files are read from */
    public static final String srcDir  = "E:\\jfuel\\src";
    
    
    
    /** 
     * Inflates all WAR files in the source directory. The 
     * destination directory is first cleared before WAR files
     * are inflated.
     */
    protected void inflate() {
        
        System.out.println("Inflating applicaions...");
    
        File src  = new File(srcDir);
        File dest = new File(destDir);
        if(!src.exists())  {  src.mkdirs(); }
        if(!dest.exists()) { dest.mkdirs(); }
        
        File[] files = dest.listFiles();
        
        for(File dstFile: files){ dstFile.delete(); }
        
        files = src.listFiles();
        
        for(File tmpFile : files){
            
            if(!tmpFile.getName().endsWith(".war")){ continue; } 
            
            System.out.println("Inflating " + tmpFile.getName());
            
            try {  inflateApplication(tmpFile); }
            
            catch (IOException ex){ System.out.println("Failed to inflate application " + tmpFile.getName()); }
        }
        
        System.out.println("Applications inflated successfully");
    }
    
    
    
    /**
     * Inflates the single WAR file passed in as the argument
     * to the destination directory.
     * 
     * @param file the WAR to be inflated
     */
    private void inflateApplication(File file) throws IOException{
    
        JarFile jarFile = new JarFile(file);
        
        Enumeration<JarEntry> jarEntries = jarFile.entries();
        
        File destination = new File(destDir+"\\"+file.getName().split(".war")[0]);
        if(! destination.exists()){
             destination.mkdir(); }
        
        while(jarEntries.hasMoreElements()){
        
            JarEntry entry = jarEntries.nextElement();
            
            File entryFile = new File(destination, entry.getName());
            
            if(!entryFile.exists()){
                entryFile.getParentFile().mkdirs();
                entryFile = new File(destination, entry.getName());
            }
            
            if(entry.isDirectory()){
               continue;
            }
            
            InputStream is = jarFile.getInputStream(entry);
            FileOutputStream os = new FileOutputStream(entryFile);
            while(is.available() > 0){
                  os.write(is.read());
            }     is.close();
                  os.close();
        }
    }
}
