
package com.aksoft.jfuel.util;

import java.security.SecureRandom;

/**
 * Utility to generate random long values.
 * 
 * @author Akqeel
 */
public class RandomTokenBuilder {
    
    private static final SecureRandom random = new SecureRandom();
        
    /**
     * Generates a random long value which has 
     * digit length length
     * 
     * @param length the digit length of the long
     * 
     * @return the generated long value as a String
     */
    public static String createToken(int length){
        
        byte[] bytearray = new byte[length];
    
        random.nextBytes(bytearray);
        
        StringBuilder token = new StringBuilder();
        for(byte digit: bytearray){
            token.append( ((int)digit)&0xFF );
        }
    
        return token.toString();
    }
    
}
