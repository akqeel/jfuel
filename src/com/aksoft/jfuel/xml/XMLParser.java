

package com.aksoft.jfuel.xml;

import java.io.IOException;
import java.io.InputStream;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

/**
 * {@link XMLParser} parses XML documents.
 * 
 * @author Akqeel
 */
public class XMLParser {
    
    private static DocumentBuilder builder;

    /** 
     * Default constructor. 
     * @throws ParserConfigurationException
     */
    public XMLParser() throws ParserConfigurationException{
        if(builder == null){
           DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
           builder = factory.newDocumentBuilder();
        }
    }
    
    /**
     * Creates a {@link org.w3c.dom.Document} from the inputstream.
     * 
     * @param is inputstream containing xml data
     * 
     * @return {@link org.w3c.dom.Document}
     */
    public Document parse(InputStream is) throws IOException, SAXException{
        
        Document document = builder.parse(is); 
        
        is.close(); return document;
    }
    
}
