
package com.aksoft.jfuel.service;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpSession;

/**
 * Manages all user sessions. 
 * 
 * @author Akqeel
 */
public class SessionManager {
    
    private final Map<String, JFuelHttpSession> sessions;

    /**
     * Default constructor. Initializes a new HashMap
     * to cache sessions.
     */
    public SessionManager() {
        this.sessions = new HashMap<>();
    }
    
    /**
     * Returns the HttpSession associated with the JSESSIONID
     * 
     * @param id JSESSIONID for the request
     * @param createSession flag to create new session if the session
     *                      for id is not found
     * @return HttpSession
     */
    public HttpSession getSession(String id, boolean createSession){
    
        JFuelHttpSession session = sessions.get(id);
        
        if(session == null && createSession){
           session = createSession();
        }  session.setLastAccessTime(new Date());
        
        return session; 
    }
    
    private JFuelHttpSession createSession(){
        return new JFuelHttpSession(SessionManager.this);
    }
    
    /**
     * Clears the session with id from the cache.
     * @param id JSESSIONID for the session
     */
    protected void removeSession(String id){
        sessions.remove(id);
    }
    
}
