

package com.aksoft.jfuel.service;

import com.aksoft.jfuel.lifecycle.ApplicationInflator;
import com.aksoft.jfuel.xml.XMLParser;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * Handles all servlet requests.
 *
 * @author Akqeel
 */
public class ServletManager {

    /* Dependancies used to manage servlets */
    private final SessionManager sessionManager;
    private final ServletResolver servletResolver;
    
    /**
     * Default constructor. 
     * @throws ParserConfigurationException
     */
    public ServletManager() throws ParserConfigurationException {
        sessionManager = new SessionManager();
        servletResolver = new ServletResolver();
    }
    
    /**
     * Handles requests made to servlets. The requested servlet
     * is initialized and its service method is executed. Finally
     * the response is committed.
     * 
     * @param request the request url
     * @param is the inputstream for the request
     * @param os the outputstream for the response
     */
    public void handleResponse(String request, InputStream is, OutputStream os) throws IOException{
        
        StringBuilder response = new StringBuilder();
        
        try {
        
            if(request.isEmpty()){ return; }

            String[] requestHeaders = request.split("\n");

            String requestUrl  = requestHeaders[0].split(" ")[1];
            String httpMethod  = requestHeaders[0].split(" ")[0];
            String servletPath = parseServletPath(requestUrl.split("\\?")[0]);
            String contextPath = requestUrl.split("/")[1];
            String queryString = requestUrl.contains("\\?") ? requestUrl.split("\\?")[1] : "";
           
            
            //create headers map from http header 
            Map<String, List<String>> map = new HashMap<>();
            for(int count = 1;  count < requestHeaders.length; count++){
                List<String> values = new ArrayList<>();
                
                String   headerName   = requestHeaders[count].split(":")[0];
                String[] headerValues = requestHeaders[count].split(":")[1].split(";");
                
                for(String value: headerValues){ 
                             values.add(value);}
                
                map.put(headerName, values);
            }
            
            HttpServlet servlet = servletResolver.resolveServlet(contextPath, servletPath);

            if(servlet == null){
                throw new FileNotFoundException();
            }
            
            JFuelRequest  jfuelRequest  = new JFuelRequest(is, sessionManager, httpMethod, new Header(map), servletPath, contextPath, queryString);
            JFuelResponse jfuelResponse = new JFuelResponse(os);
            
            servlet.service(jfuelRequest, jfuelResponse);
            
            jfuelResponse.commit();
            
        } catch(FileNotFoundException ex){
            response.append("HTTP/1.1 404 Not Found\r\n")
                    .append("Date: ").append(Header.DATE_FORMAT.format(new Date())).append("\r\n")
                    .append("Content-Type: text/html\r\n")
                    .append("Content-:Length: 200\r\n")
                    .append("\r\n")
                    .append("<html><head><title>Page not found</title></head><body>The page was not found.</body></html>");
                  os.write(response.toString().getBytes());
            
        } catch (IOException | SAXException | ClassNotFoundException | ServletException ex){
            response.append("HTTP/1.1 500 Internal Server Error\n")
                    .append("Date: ").append(Header.DATE_FORMAT.format(new Date())).append("\r\n")
                    .append("Content-Type: text/html\r\n")
                    .append("Content-:Length: 200\r\n")
                    .append("\r\n")
                    .append("<html><head><title>JFuel Internal Server Error</title></head><body>Oops, something went wrong on the server.</body></html>");
                  os.write(response.toString().getBytes());
        }
    }
    
    
    
    /**
     * Initializes the servlet which is requested. {@link ServletResolver}
     * uses the context path to locate the application the servlet lies in, 
     * and the servlet path to find the servlet based on the application's
     * deployment descriptor
     */
    private static class ServletResolver{
    
        private final XMLParser xmlParser;
        private final Map<String, Servlet> servletCache;

        /**
         * Default constructor.
         * 
         * @throws ParserConfigurationException
         */
        public ServletResolver() throws ParserConfigurationException {
            this.xmlParser = new XMLParser();
            this.servletCache = new HashMap<>();
        }
        
        /**
         * Initializes the servlet based on the context path
         * and servlet path
         * 
         * @param contextPath the context path of the request URL
         * @param servletPath the servlet path of the request URL
         * 
         * @return the initialized HttpServlet
         * 
         * @throws FileNotFoundException
         * @throws ClassNotFoundException
         * @throws IOException
         * @throws SAXException
         */
        private HttpServlet resolveServlet(String contextPath, String servletPath) throws FileNotFoundException, IOException, SAXException, ClassNotFoundException{
    
            StringBuilder contextUri = new StringBuilder()
                    .append(ApplicationInflator.destDir)
                    .append(File.separator)
                    .append(contextPath)
                    .append(File.separator);
            
            StringBuilder ddUri = new StringBuilder()
                    .append(contextUri.toString())
                    .append("WEB-INF")
                    .append(File.separator)
                    .append("web.xml");
            
            File file = new File(ddUri.toString());
            
            if(!file.exists()){ 
                throw new FileNotFoundException();
            }
            
            Document document = xmlParser.parse(new FileInputStream(file));
            
            String className = resolveServletName(document, servletPath);
            
            HttpServlet servlet = (HttpServlet) servletCache.get(contextPath.concat(className));
            
            if(servlet != null){ return servlet; }
            
            
            StringBuilder classUriBuilder = new StringBuilder()
                    .append(contextUri.toString())
                    .append("WEB-INF")
                    .append(File.separator)
                    .append("classes");
            
            File classDir = new File(classUriBuilder.toString());
            
            URLClassLoader classLoader = URLClassLoader.newInstance(new URL[] { classDir.toURI().toURL() });
            
            Class clazz = Class.forName(className, true, classLoader);
            
            
            try {
            
                servlet = (HttpServlet) clazz.newInstance();
                
                servlet.init();
            
                servletCache.put(contextPath.concat(className), servlet);
                
            } catch (Exception ex){ return null; }
            
              return servlet;
        }
        
        
        
        /**
         * Parses the deployment descriptor to locate the servlet
         * 
         * @param document the deployment descriptor for the application
         * @param servletPath the servlet path of the request URL
         * 
         * @return the fully qualified name of the servlet
         * 
         * @throws FileNotFoundException
         */
        private String resolveServletName(Document document, String servletPath) throws FileNotFoundException{
        
            int len = document.getElementsByTagName("servlet-mapping").getLength();
            
            
            NodeList nodeList = null;
            outer: for(int idx = 0; idx < len; idx++){
                NodeList nodes = document.getElementsByTagName("servlet-mapping").item(idx).getChildNodes();
                for(int child = 0; child < nodes.getLength(); child++){
                    if(nodes.item(child).getTextContent().equals(servletPath)){
                        nodeList = nodes; break outer;
                    } 
                }
            }
            
            if(nodeList == null){ throw new FileNotFoundException(); } 
            
            
            String servletName = null;
            for(int child = 0; child < nodeList.getLength(); child++){
                if(nodeList.item(child).getNodeName().equals("servlet-name")){
                   servletName = nodeList.item(child).getFirstChild().getTextContent(); break;
                }
            }
            
            if(servletName == null){ throw new FileNotFoundException(); }
            
            
            len = document.getElementsByTagName("servlet").getLength();
            
            nodeList = null;
            outer: for(int idx = 0; idx < len; idx++){
                NodeList nodes = document.getElementsByTagName("servlet").item(idx).getChildNodes();
                for(int child = 0; child < nodes.getLength(); child++){
                    if(nodes.item(child).getNodeName().equals("servlet-name") &&
                       nodes.item(child).getFirstChild().getTextContent().equals(servletName)){
                       nodeList = nodes; break outer;
                    }
                }
            }
            
            if(nodeList == null){ throw new FileNotFoundException(); }
            
            
            String className = null;
            for(int idx = 0; idx < nodeList.getLength(); idx++){
                if(nodeList.item(idx).getNodeName().equals("servlet-class")){
                   className = nodeList.item(idx).getFirstChild().getTextContent(); break;
                }
            }
            
            if(className == null){ throw new FileNotFoundException(); } 
            
            return className;
        }
    }
    
    /**
     * Extacts the servlet path from the request URI.
     * 
     * @param uri the request URI
     * 
     * @return the servlet path for this request
     */
    private String parseServletPath(String uri){
        
        String[] dirs = uri.split("/");
        
        StringBuilder servletPath = new StringBuilder();
        
        for(int count = 2; count < dirs.length; count++){
            servletPath.append("/")
                       .append(dirs[count]);
        }  
        
        return servletPath.toString();
    }
    
}
