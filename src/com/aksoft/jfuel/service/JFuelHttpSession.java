
package com.aksoft.jfuel.service;

import com.aksoft.jfuel.adapter.EnumerationAdapter;
import com.aksoft.jfuel.util.RandomTokenBuilder;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionContext;

/**
 * HttpSession implementation used to manage
 * user session.
 * 
 * @author Akqeel
 */
public class JFuelHttpSession implements HttpSession{

    private final SessionManager sessionManager;
    private final Date creationDate;
    private final Map<String, Object> attributes; 
    private final Map<String, Object> values; 
    private final String sessionId; 
    private Date lastAccessDate;
    private int maxInactiveInterval;
    
    
    /**
     * Constructor taking a {@link SessionManager} instance. The
     * SessionManager is used to register and unregister this
     * {@link JFuelHttpSession}
     * 
     * @param sessionManager {@link SessionManager} instance used to register and unregister this session
     */
    public JFuelHttpSession(SessionManager sessionManager) {
        this.sessionManager = sessionManager;
        this.creationDate = new Date();
        this.attributes = new HashMap<>();
        this.values = new HashMap<>();
        this.sessionId = generateId(); 
    }
    
    
    /**
     * Generates a random id which is used for
     * the JSESSIONID to identify a user's {@link Cookie}
     */
    private String generateId(){
        
        String id = null;
        
        do{ id = RandomTokenBuilder.createToken(6); }
        
        while(sessionManager.getSession(id, false) != null); return id;
    }
    
    @Override
    public long getCreationTime() {
        return creationDate.getTime();
    }

    @Override
    public String getId() {
        return sessionId;
    }

    @Override
    public long getLastAccessedTime() {
        return lastAccessDate.getTime();
    }

    @Override
    public ServletContext getServletContext() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setMaxInactiveInterval(int i) {
        this.maxInactiveInterval = i;
    }

    @Override
    public int getMaxInactiveInterval() {
        return maxInactiveInterval;
    }

    @Override
    public HttpSessionContext getSessionContext() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object getAttribute(String string) {
        return attributes.get(string);
    }

    @Override
    public Object getValue(String string) {
        return values.get(string);
    }

    @Override
    public Enumeration<String> getAttributeNames() {
        return new EnumerationAdapter(attributes.keySet().iterator());
    }

    @Override
    public String[] getValueNames() {
        return (String[]) values.keySet().toArray();
    }

    @Override
    public void setAttribute(String string, Object o) {
        attributes.put(string, o);
    }

    @Override
    public void putValue(String string, Object o) {
        values.put(string, o);
    }

    @Override
    public void removeAttribute(String string) {
        attributes.remove(string);
    }

    @Override
    public void removeValue(String string) {
        values.remove(string);
    }

    @Override
    public void invalidate() {
        attributes.clear();
        values.clear();
        sessionManager.removeSession(sessionId);
    }

    @Override
    public boolean isNew() {
        return getCreationTime() == getLastAccessedTime();
    }
    
    protected void setLastAccessTime(Date date){
        this.lastAccessDate = date;
    }
    
}
