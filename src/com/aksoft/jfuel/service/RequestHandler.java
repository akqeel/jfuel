
package com.aksoft.jfuel.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Socket;
import javax.xml.parsers.ParserConfigurationException;

/**
 * Handles all requests made to the server.
 * Any request received by {@link PortListener}
 * is delegated here.
 *
 * @author Akqeel
 */
public class RequestHandler {

    
    /* {@link ServletManager} used to handle
     * requests made to servlets. */
    private final ServletManager servletManager;

    
    
    /**
     * Default constructor for this {@link RequestHandler}
     * {@link ServletManager} is initialized here.
     * 
     * @throws ParserConfigurationException
     */
    public RequestHandler() throws ParserConfigurationException {
        servletManager = new ServletManager();
    }

    
    /**
     * Creates a new Thread which handles the request.
     * Implements concurrency
     */
    public void handleRequest(Socket socket) throws IOException {
    
        new RequestHandlerThread(socket).start();
    }

    
                
    private class RequestHandlerThread extends Thread {

        private final Socket socket;

        public RequestHandlerThread(Socket socket) {
            this.socket = socket;
        }

        @Override
        public void run() {

            try {
                InputStream is = socket.getInputStream();
                
                BufferedReader httpReader = new BufferedReader(new InputStreamReader(is));

                StringBuilder request = new StringBuilder();
                
                String str = null;
                while ((str = httpReader.readLine()) != null && !str.equals("")) {
                    request.append(str).append("\n");
                }

                OutputStream os = socket.getOutputStream();
                
                servletManager.handleResponse(request.toString(), is, os);

                os.flush();
                os.close();
                httpReader.close();
                socket.close();

            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

}
