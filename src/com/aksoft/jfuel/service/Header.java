
package com.aksoft.jfuel.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.servlet.http.Cookie;

/**
 * POJO representing a HTTP header.
 * This implementation makes it easier to work
 * with request and response headers.
 * 
 * @author Akqeel
 */
public class Header {
    
    public static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dddd, dd mmmm yyyy hh:MM:ss z");
    public static final String HEADER_STATUS = "Status";
    public static final String HEADER_DATE = "Date: ";
    public static final String HEADER_CONTENT_TYPE = "Content-Type: ";
    public static final String HEADER_CONTENT_LENGTH = "Content-Length: ";
    public static final String HEADER_CONTENT = "Content: ";
    public static final String HEADER_LOCATION = "Location: ";
    public static final String HEADER_CHAR_ENCODING = "Character-Encoding: ";
    
    private final Map<String, List<String>> headers;
    private final Map<String, Cookie> cookies;

    /** Default constructor. */
    protected Header(){
        this.headers = new HashMap<>();
        this.cookies = new HashMap<>();
    }
    
    /** 
     * Constructor which initializes this instance with
     * a predefined Map of header values.
     * 
     * @param headers Map holding header name-value pairs
     */
    protected Header(Map<String, List<String>> headers){
        this.headers = headers;
        this.cookies = new HashMap<>();
    }
    
    
    protected void addCookie(Cookie cookie){
        if(cookies.get(cookie.getName()) != null){
           cookies.remove(cookie.getName());
        }  cookies.put(cookie.getName(), cookie);
    }
    
    protected Cookie[] getCookies(){
        return (Cookie[]) cookies.values().toArray();
    }
    
    protected void setHeader(String name, String value){
        if(containsHeader(name)){
             removeHeader(name);} 
        
        List<String> values = new ArrayList<>();
        values.add(name);
        headers.put(name, values);
    }
    
    protected void addHeader(String name, String value){
        List<String> values = headers.get(name);
        
        if(values == null){ setHeader(name, value); }  
        
                     else {      values.add(value); }
    }
    
    protected List<String> getHeaders(String name){
        return headers.get(name);
    }
    
    protected Set<String> getHeaderNames(){
        return headers.keySet();
    }
    
    protected boolean containsHeader(String name){
        return headers.containsKey(name);
    }
    
    private void removeHeader(String name){
        headers.remove(name);
    }
    
}
