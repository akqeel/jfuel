
package com.aksoft.jfuel.service;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

/**
 * The response associated with a request. 
 * The contents of this response is sent
 * back to the client.
 * 
 * @author Akqeel
 */
public class JFuelResponse implements HttpServletResponse{

    private final OutputStream outputStream;
    private final ByteArrayOutputStream outputStreamBuffer;
    private final ServletOutputStream servletOutputStream;
    private final PrintWriter printWriter;
    private final Header header;
    
    /* Flag to track response state */
    private boolean isCommitted = false;
    
    /**
     * Constructor for this {@link JFuelResponse}. 
     * An additional output stream is initialized in the constructor
     * which acts as a buffer. When committing the data, the buffer is copied
     * to the outputstream which in turn writes data back to the client.
     * 
     * Do not write the response directly to the outputstream as it will be sent to
     * the client without any headers.
     * 
     * @param outputStream outputstream used to write data back to the client
     */
    public JFuelResponse(OutputStream outputStream) {
        this.outputStream = outputStream;
        this.header = new Header();
        this.outputStreamBuffer = new ByteArrayOutputStream();
        
        this.servletOutputStream = new ServletOutputStream() {
            @Override 
            public void write(int i) throws IOException {
                if(isCommitted){ return; }
                outputStreamBuffer.write(i);
            }
        };
        
        this.printWriter = new PrintWriter(servletOutputStream);
    }
    
    @Override
    public void addCookie(Cookie cookie) {
        header.addCookie(cookie);
    }

    @Override
    public boolean containsHeader(String string) {
        return header.containsHeader(string);
    }

    @Override
    public String encodeURL(String string) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String encodeRedirectURL(String string) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String encodeUrl(String string) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String encodeRedirectUrl(String string) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void sendError(int i, String string) throws IOException {
    
        StringBuilder builder = new StringBuilder()
                .append(Integer.toString(i))
                .append(" ")
                .append(string);
        header.setHeader(Header.HEADER_STATUS, builder.toString());
        header.setHeader(Header.HEADER_CONTENT_TYPE, "text/html");
        resetBuffer();
        outputStreamBuffer.write("<html><head><title>JFuel Internal Server Error</title></head><body>Oops, something went wrong on the server.</body></html>".getBytes());
        commit();
    }

    @Override
    public void sendError(int i) throws IOException {
        sendError(i, "");
    }

    @Override
    public void sendRedirect(String string) throws IOException {
        header.setHeader(Header.HEADER_LOCATION, string);
        resetBuffer();
        commit();
    }

    @Override
    public void setDateHeader(String string, long l) {
        Date date = new Date(l);
        header.setHeader(string, Header.DATE_FORMAT.format(date));
    }

    @Override
    public void addDateHeader(String string, long l) {
        Date date = new Date(l);
        header.addHeader(string, Header.DATE_FORMAT.format(date));
    }

    @Override
    public void setHeader(String string, String string1) {
        header.setHeader(string, string1);
    }

    @Override
    public void addHeader(String string, String string1) {
        header.addHeader(string, string1);
    }

    @Override
    public void setIntHeader(String string, int i) {
        header.setHeader(string, Integer.toString(i));
    }

    @Override
    public void addIntHeader(String string, int i) {
        header.addHeader(string, Integer.toString(i));
    }

    @Override
    public void setStatus(int i) {
        header.setHeader(Header.HEADER_STATUS, Integer.toString(i));
    }

    @Override
    public void setStatus(int i, String string) {
        StringBuilder builder = new StringBuilder()
                .append(Integer.toString(i))
                .append(" ")
                .append(string);
        header.setHeader(Header.HEADER_STATUS, builder.toString());
    }

    @Override
    public int getStatus() {
        List<String> statusLines = header.getHeaders(Header.HEADER_STATUS);
        
        if(statusLines == null){ return 0; }
        
        return Integer.parseInt(statusLines.get(0).split(" ")[0]);
    }

    @Override
    public String getHeader(String string) {
        List<String> values = header.getHeaders(string);
        
        if(values != null){ return values.get(0); }
                            return null;
    }

    @Override
    public Collection<String> getHeaders(String string) {
        return header.getHeaders(string);
    }

    @Override
    public Collection<String> getHeaderNames() {
        return header.getHeaderNames();
    }

    @Override
    public String getCharacterEncoding() {
        List<String> values = header.getHeaders(Header.HEADER_CHAR_ENCODING);
        
        if(values != null){ return values.get(0); }
                            return null;
    }

    @Override
    public String getContentType() {
        List<String> values = header.getHeaders(Header.HEADER_CONTENT_TYPE);
        
        if(values != null){ return values.get(0); }
                            return null;
    }

    @Override
    public ServletOutputStream getOutputStream() throws IOException {
        return servletOutputStream;
    }

    @Override
    public PrintWriter getWriter() throws IOException {
        return printWriter;
    }

    @Override
    public void setCharacterEncoding(String string) {
        header.setHeader(Header.HEADER_CHAR_ENCODING, string);
    }

    @Override
    public void setContentLength(int i) {
        header.setHeader(Header.HEADER_CONTENT_LENGTH, Integer.toString(i));
    }

    @Override
    public void setContentType(String string) {
        header.setHeader(Header.HEADER_CONTENT_TYPE, string);
    }

    @Override
    public void setBufferSize(int i) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getBufferSize() {
        return outputStreamBuffer.size();
    }

    @Override
    public void flushBuffer() throws IOException {
        outputStreamBuffer.flush();
    }

    @Override
    public void resetBuffer() {
        outputStreamBuffer.reset();
    }

    @Override
    public boolean isCommitted() {
        return isCommitted;
    }

    @Override
    public void reset() {
        outputStreamBuffer.reset();
    }

    @Override
    public void setLocale(Locale locale) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Locale getLocale() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    protected ByteArrayOutputStream getOutputStreamBuffer(){
        return outputStreamBuffer;
    }
    
    /**
     * Commits the response to the client. First the headers are read
     * from {@link Header} and written to the outputstream. Then the 
     * data from the buffer is written to the outputstream.
     * 
     * Note that this method does nothing if the response has already
     * been committed.
     */
    protected void commit(){
        
        try {
            if(isCommitted){ return; } 
            
            byte[] responseContent = outputStreamBuffer.toByteArray();
            
            StringBuilder responseHeader = new StringBuilder()
                .append("HTTP/1.1 ").append(getHeader(Header.HEADER_STATUS)).append("\r\n")
                .append("Date: ").append(Header.DATE_FORMAT.format(new Date())).append("\r\n")
                .append("Content-Type: ").append(getHeader(Header.HEADER_CONTENT_TYPE)).append("\r\n")
                .append("Content-:Length: ").append(responseContent.length).append("\r\n")
                .append("\r\n");
            
            outputStream.write(responseHeader.toString().getBytes());
            outputStream.write(responseContent);
            outputStream.flush();
            outputStream.close();
            isCommitted = true;
            
        } catch (IOException ex) {}
    }

}
