JFuel 0.6.0

JFuel is a JEE Application Server designed for deploying and serving WAR content. The current     version has a functioning servlet engine and can service client requests made to pure servlet (not JSP) implementations.

Blog: https://akqeel.wordpress.com/2015/05/01/87/

Source: https://bitbucket.org/akqeel/jfuel/src/a25454f9d72ede6d104a846298a5a1c70a79df61?at=master